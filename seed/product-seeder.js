var Product = require('../models/product');
var mongoose = require('mongoose');

mongoose.connect('localhost:27017/yo2'); 

console.log('Mongoose connected');

var product = new Product({
firstName: 'Udit',
lastName: 'Setia'
});
product.save(); 

console.log('Data added');

mongoose.disconnect();

console.log('Mongoose Disconnected');


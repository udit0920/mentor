var mongoose =require('mongoose');
var Schema= mongoose.Schema;

var mentorSchema=new Schema({
	firstName: {type:String,required:true},
	lastName: {type:String,required:true},
	phoneNumber: {type:Number,required:true},
	email:{type:String,required:true}
	// userName:{type:String,required:true}

});

module.exports=mongoose.model('Mentor',mentorSchema);
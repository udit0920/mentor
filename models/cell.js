var mongoose =require('mongoose');
var Schema= mongoose.Schema;

var storeCell=new Schema({
	i: {type:Number,required:true},
	j: {type:Number,required:true},	
});

module.exports=mongoose.model('Cell',storeCell);
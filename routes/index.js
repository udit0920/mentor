var express = require('express');
var router = express.Router();
var passport=require('passport');
var flash = require('connect-flash');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('menu');
});

router.get('/slots',function(req,res,next){
res.render('slots');

});

router.post('/studentPage', function(req,res,next){

res.render('studentPage')
});

router.post('/mentorPage', function(req,res,next){

res.render('mentorPage')
});

router.get('/mentorPage', function(req, res, next) {
	var messages =req.flash('error');
  res.render('mentorPage',{messages:messages,hasErrors:messages.length>0});
});

router.get('/studentPage', function(req, res, next) {
	var messages =req.flash('error');
  res.render('studentPage',{messages:messages,hasErrors:messages.length>0});
});



router.get('/studentRegisteredSuccess', function(req, res, next) {
  res.render('studentRegisteredSuccess');
});

router.get('/mentorRegisteredSuccess', function(req, res, next) {
  res.render('mentorRegisteredSuccess');
});

router.post('/insert',function(req,res){

var i= req.params[0];
var j=req.params[1];

console.log("II="+i);
console.log("JJ="+j);
});

router.post('/submitStudentDetails',passport.authenticate('local',{

successRedirect:  'studentRegisteredSuccess',
failureRedirect:  'studentPage',
failureFlash: true
}));

router.post('/submitMentorDetails',passport.authenticate('local-mentor',{

successRedirect:  'mentorRegisteredSuccess',
failureRedirect:  'mentorPage',
failureFlash: true
}));


module.exports = router;
